@extends('layout')
@section('title') Add New Book - Booxtant @stop
@section('page-title')
Add New Book
@stop
@section('page-content')
     <div class="main-content-container container">
                <div class="row">

                    <div class="col-md-12">
                        <div id="content" class="main-content-inner" role="main">

                            <article id="post-1709" class="post-1709 page type-page status-publish entry">

                                <div class="entry-content">

                                    <div class="woocommerce">

                                    <h2>Add New Book!</h2>

                                    @if (count($errors) > 0)
                                        <div class="alert alert-danger">
                                          <ul>
                                            @foreach ($errors->all() as $error)
                                              <li>{{ $error }}</li>
                                            @endforeach
                                          </ul>
                                       </div>
                                    @endif
                                    
                                    {!! Form::open(array('route' => 'storeBook', 'class' => 'login', 'novalidate' => 'novalidate', 'files' => true))!!}
                                            <p class="woocommerce-FormRow woocommerce-FormRow--wide form-row form-row-wide">
                                            {!! Form::label('name', 'Book Name') !!}
                                            {!!Form::text('name',null, array('class' => 'woocommerce-Input woocommerce-Input--text input-text', 'required'))!!}
                                            </p>

                                            <p class="woocommerce-FormRow woocommerce-FormRow--wide form-row form-row-wide">
                                            {!! Form::label('description', 'Enter Book Description') !!}
                                            {!!Form::text('description',null, array('class' => 'woocommerce-Input woocommerce-Input--text input-text', 'required'))!!}
                                            </p>
                                            <p class="woocommerce-FormRow woocommerce-FormRow--wide form-row form-row-wide">
                                            {!! Form::label('book_cover', 'Select Book Cover') !!}
                                            {!! Form::file('book_cover', null, array('class' => 'woocommerce-Input woocommerce-Input--text input-text', 'required')) !!}
                                            </p>
                                            <p class="woocommerce-FormRow woocommerce-FormRow--wide form-row form-row-wide">
                                            {!! Form::label('book_pdf', 'Select PDF') !!}
                                            {!! Form::file('book_pdf', null, array('class' => 'woocommerce-Input woocommerce-Input--file input-file', 'required')) !!}
                                            </p>
                                            <p class="form-row">
                                                <input type="submit" class="woocommerce-Button button" name="login" value="Create A New Book" />
                                                {{-- <label for="rememberme" class="inline">
                                                    <input class="woocommerce-Input woocommerce-Input--checkbox" name="rememberme" type="checkbox" id="rememberme" value="forever" /> Remember me </label> --}}
                                            </p>
                                         {!! Form::close()!!}
                                        

                                    </div>

                                </div>

                            </article>

                        </div>
                    </div>
                </div>
            </div>
@stop