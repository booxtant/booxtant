@extends('layout')
@section('title') Account - Booxtant @stop
@section('page-title')
Account
@stop
@section('page-content')
<div class="main-content-container container"> <div class="row"> <div class="col-md-12"> <div id="content" class="main-content-inner" role="main"> <article id="post-1707" class="post-1707 page type-page status-publish entry"> <div class="entry-content"> <div class="woocommerce"> <table class="shop_table shop_table_responsive cart" cellspacing="0"> <thead> <tr> <th class="product-name">Remove</th> <th class="product-name">Book Cover</th> <th class="product-name">Book Name</th> </tr></thead> <tbody>
                                                   @foreach ($books as $book)
                                                    <tr class="cart_item">
                                                        <td class="product-remove">
                                                            {!! Form::open(['method' => 'DELETE', 'route' => ['books.destroy', $book->id]]) !!}
                                                                {!! Form::submit('Remove', ['class' => 'btn btn-danger']) !!}
                                                            {!! Form::close() !!} 
                                                        </td>

                                                        <td class="product-thumbnail">
                                                            <a href="#"><img width="102" height="136" src="/{{$book->book_cover}}" class="attachment-shop_thumbnail size-shop_thumbnail wp-post-image" alt="book-15"></a>
                                                        </td>

                                                        <td class="product-name" data-title="Product">
                                                            <a href="#">{{$book->name}}</a> </td>
                                                    </tr>
                                                   @endforeach</tbody> </table> </div></article> </div></div></div></div>
@stop