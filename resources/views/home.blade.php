<!DOCTYPE html>
<html lang="en-US">

<head> @section('title') Home - Booxtant @stop @include('partials.meta') @include('partials.fonts') @include('partials.styles') @include('partials.head-scripts') </head>

<body class="home page page-template page-template-page_visual_composer page-template-page_visual_composer-php header-large ltr has_slider header_absolute wpb-js-composer js-comp-ver-4.12 vc_responsive">
    <div class="site-wrap">
        <header class="section-site-header">
            <div class="site-header">
                <div class="container">
                    <h1 class="site-brand pull-left"> <a class="navbar-brand" href="{{URL::route('index')}}"> <div class="site-logo-text"> <span class="vc_icon_element-icon entypo-icon entypo-icon-book-open logo-icon"></span> </div><p class="site-description">Booxtant</p></a> </h1>
                    <div class="site-quicknav pull-right">
                        <ul class="nav navbar-right">
                            <li class="dropdown visible-xs visible-sm">
                                <a href="#" class="dropdown-toggle" data-toggle="collapse" data-target=".navbar-collapse-top"> <i class="fa fa-navicon"></i> </a>
                            </li>@if (Auth::check())
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle cart-subtotal" data-toggle="dropdown" rel="nofollow"> <span class="topnav-label"><span class="amount">Welcome, {{Auth::user()->firstname}}</span></span> <i class="simple-icon-user"></i> </a>
                                <ul class="dropdown-menu topnav-minicart-dropdown sm-nowrap">
                                    <li class="menu-item"><a title="My Account" href="{{URL::route('account')}}">My Books</a></li>
                                    <li class="menu-item"><a title="My Account" href="{{URL::route('books.create')}}">Add A New Book</a></li>
                                    <li class="menu-item"><a title="My Account" href="{{URL::route('users.logout')}}">Logout</a></li>
                                </ul>
                            </li>@endif @if (!Auth::check())
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle cart-subtotal" data-toggle="dropdown" rel="nofollow"> <span class="topnav-label"><span class="amount">Welcome, Guest</span></span> <i class="simple-icon-user"></i> </a>
                                <ul class="dropdown-menu topnav-minicart-dropdown sm-nowrap">
                                    <li class="menu-item"><a title="My Account" href="{{URL::route('login')}}">SIGN IN</a></li>
                                    <li class="menu-item"><a title="My Account" href="{{URL::route('join')}}">JOIN</a></li>
                                </ul>
                            </li>@endif </ul>
                    </div>
                    <div class="site-menu navbar-collapse collapse navbar-collapse-top ">
                        <ul id="menu-header-menu" class="site-menu nav navbar-nav">
                            <li class="menu-item"><a title="Home" href="{{URL::route('index')}}">Home</a> </li>
                            <li class="menu-item"><a title="about" href="{{URL::route('index')}}">Who We Are</a> </li>
                            <li class="menu-item"><a title="Bookstore" href="{{URL::route('explore')}}">Explore Books</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </header>
        <div class="toko-slider-wrap">
            <div class="toko-slides toko-slider-active owl-carousel owl-theme owl-loaded">
                <div class="toko-slide" style="background-image:url(images/slider-01.jpg);background-size:cover;background-position:center right;background-repeat:no-repeat;">
                    <div class="toko-slide-inner">
                        <div class="toko-slide-detail">
                            <p class="toko-slide-desc">If there's a book that you want to read, but it hasn't been written yet, then you must write it.</p>
                            <h4 class="toko-slide-title">Let's Get Started</h4>
                            @if (!Auth::check())
                            <a class="toko-slide-button" href="{{URL::route('join')}}">Learn More</a>
                            @endif
                            @if (Auth::check())
                            <a class="toko-slide-button" href="{{URL::route('books.create')}}">Add A Book</a>
                            @endif

                        </div>
                    </div>
                </div>
                <div class="toko-slide" style="background-image:url(images/slider-02.jpg);background-size:cover;background-position:center right;background-repeat:no-repeat;">
                    <div class="toko-slide-inner">
                        <div class="toko-slide-detail">
                            <p class="toko-slide-desc">If you only read the books that everyone else is reading, you can only think what everyone else is thinking</p>
                            <h4 class="toko-slide-title">Explore Books</h4><a class="toko-slide-button" href="{{URL::route('explore')}}">Explore</a></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="main-content">
            <div class="main-content-container container">
                <div id="content" class="main-content-inner" role="main">
                    <article id="post-701" class="post-701 page type-page status-publish entry">
                        <div class="entry-content">
                            <div data-vc-full-width="true" data-vc-full-width-init="false" data-vc-stretch-content="true" class="vc_row wpb_row vc_row-fluid vc_row-no-padding">
                                <div class="wpb_column vc_column_container vc_col-sm-12">
                                    <div class="vc_column-inner ">
                                        <div class="wpb_wrapper">
                                            <div class="books-search">
                                                <div class="container">{!! Form::open(array('method'=>'GET','route'=> 'search','autocomplete'=>'off')) !!}
                                                    <div class="row">
                                                        <div class="col-sm-9 col-md-9">
                                                            <div class="form-group">{!! Form::text('search', null, array('required', 'class'=>'form-control','placeholder'=>'Search Books')) !!}</div>
                                                        </div>
                                                        <div class="col-sm-3 col-md-3">
                                                            <div class="form-group">{!! Form::submit('search',array('class'=>'btn btn-primary btn-block')) !!}</div>
                                                        </div>
                                                    </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="vc_row-full-width vc_clearfix"></div>
                            <div data-vc-full-width="true" data-vc-full-width-init="false" class="vc_row wpb_row vc_row-fluid vc_custom_1453085217674 vc_row-has-fill">
                                <div class="wpb_column vc_column_container vc_col-sm-12">
                                    <div class="vc_column-inner ">
                                        <div class="wpb_wrapper">
                                            <div class="toko-divider text-center line-no icon-no">
                                                <div class="divider-inner" style="background-color: #edf3f4">
                                                    <h3 class="toko-section-title">Recent Additions</h3> </div>
                                            </div>
                                            <div class="toko-woocommerce woocommerce columns-4 toko-no-carousel clearfix">
                                                <ul class="products " id="toko-carousel-263"> @foreach ($books as $book)
                                                    <li class="product">
                                                        <div class="product-inner">
                                                            <a href="{{URL::to('books',array('id'=>$book->id))}}" class="woocommerce-LoopProduct-link">
                                                                <figure class="product-image-box"> <span class="onsale">Recently Added</span> <img width="250" height="333" src="{{$book->book_cover}}" class="attachment-shop_catalog size-shop_catalog wp-post-image" alt="" title=""></figure>
                                                                <div class="product-price-box clearfix">
                                                                    <h4>{{$book->name}}</h4><span class="person-name vcard">Published By : </span> </del> <ins><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol"></span>{{$book->user->firstname}} {{$book->user->lastname}}</span></ins></span>
                                                                </div>
                                                            </a>
                                                            <div class="woo-button-wrapper">
                                                                <div class="woo-button-border"><a href="{{URL::to('books',array('id'=>$book->id))}}" class="button product-button">Detail</a><!-- -->@if(Auth::check())<!-- --><a rel="nofollow" data-pdf="{{$book->book_pdf}}" class="showpdf button product_type_simple add_to_cart_button ajax_add_to_cart " data-toggle="modal" data-target="#modal-fullscreen">Read</a> @endif<!-- -->@if(!Auth::check())<!-- --><a href="{{URL::route('join')}}" rel="nofollow" class="showpdf button product_type_simple add_to_cart_button ajax_add_to_cart ">Join To Read</a> @endif </div>
                                                            </div>
                                                        </div>
                                                    </li>@endforeach </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="vc_row-full-width vc_clearfix"></div>
                            <div data-vc-full-width="true" data-vc-full-width-init="false" class="vc_row wpb_row vc_row-fluid vc_custom_1453092599145 vc_row-has-fill">
                                <div class="wpb_column vc_column_container vc_col-sm-6">
                                    <div class="vc_column-inner ">
                                        <div class="wpb_wrapper">
                                            <div class="vc_icon_element vc_icon_element-outer vc_icon_element-align-center">
                                                <div class="vc_icon_element-inner vc_icon_element-color-custom vc_icon_element-size-xl vc_icon_element-style- vc_icon_element-background-color-grey"><span class="vc_icon_element-icon entypo-icon entypo-icon-book-open" style="color:#27c8ea !important"></span></div>
                                            </div>
                                            <h2 style="font-size: 18px;text-align: center;font-family:Montserrat;font-weight:400;font-style:normal" class="vc_custom_heading">Explore New Stuff</h2>
                                            <div class="wpb_text_column wpb_content_element ">
                                                <div class="wpb_wrapper">
                                                    <p style="text-align: center;">Explore New Ideas from the minds of future Paulo Coelho</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="wpb_column vc_column_container vc_col-sm-6">
                                    <div class="vc_column-inner ">
                                        <div class="wpb_wrapper">
                                            <div class="vc_icon_element vc_icon_element-outer vc_icon_element-align-center">
                                                <div class="vc_icon_element-inner vc_icon_element-color-custom vc_icon_element-size-xl vc_icon_element-style- vc_icon_element-background-color-grey"><span class="vc_icon_element-icon entypo-icon entypo-icon-pencil" style="color:#86e154 !important"></span></div>
                                            </div>
                                            <h2 style="font-size: 18px;text-align: center;font-family:Montserrat;font-weight:400;font-style:normal" class="vc_custom_heading">Submit Your Work</h2>
                                            <div class="wpb_text_column wpb_content_element ">
                                                <div class="wpb_wrapper">
                                                    <p style="text-align: center;">Want to submit your creation ?
                                                        <br/>Booxtant is the platform just for you. Publish and target your work to a large audience through Booxtant.</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="vc_row-full-width vc_clearfix"></div>
                            @if(Auth::check())
                            @if(Auth::user()->id == 2)
                            <div data-vc-full-width="true" data-vc-full-width-init="false" class="vc_row wpb_row vc_row-fluid vc_custom_1453085217674 vc_row-has-fill">
                                <div class="wpb_column vc_column_container vc_col-sm-12">
                                    <div class="vc_column-inner ">
                                        <div class="wpb_wrapper">
                                            <div class="toko-divider text-center line-no icon-no">
                                                <div class="divider-inner" style="background-color: #edf3f4">
                                                    <h3 class="toko-section-title">Our Source Code Link For : </h3>
                                                </div>
                                                <div class="divider-inner" style="background-color: #edf3f4">
                                                    <img src="images/mmu.png"/> 
                                                </div>
                                                <div class="divider-inner" style="background-color: #edf3f4">
                                                    <a href="#"><img src="images/git.png" style="width:30%" /></a> 
                                                </div>

                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="vc_row-full-width vc_clearfix"></div>
                            @endif
                            @endif
                            <div data-vc-full-width="true" data-vc-full-width-init="true" class="vc_row wpb_row vc_row-fluid vc_custom_1466502685722 vc_row-has-fill" style="position: relative; left: -366.5px; box-sizing: border-box; width: 1903px; padding-left: 366.5px; padding-right: 366.5px;">
                                <div class="wpb_column vc_column_container vc_col-sm-12">
                                    <div class="vc_column-inner ">
                                        <div class="wpb_wrapper">
                                            <div class="toko-cta text-center">
                                                <h2 class="toko-cta-title">Browse Through Our Complete Library</h2> <a href="{{URL::route('explore')}}" class="toko-cta-link">BROWSE COLLECTION</a> </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="vc_row-full-width vc_clearfix"></div>
                        </div>
                    </article>
                </div>
            </div>
        </div>
        <footer class="section-site-footer">
            <div class="site-footer">
                <div class="container">
                    <div class="footer-credit">
                        <p>Copyright &copy; Booxtant - 2016</p>
                    </div>
                </div>
            </div>
        </footer>
    </div>
    <div class="modal modal-fullscreen fade" id="modal-fullscreen" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                </div>
                <div class="modal-body">
                    <div id="flipbookContainer"></div>
                </div>
            </div>
        </div>
    </div>{{--
    <div id="flipbookContainer"></div>--}}@include('partials.bottom-scripts')</body>

</html>