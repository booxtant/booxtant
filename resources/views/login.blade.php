@extends('layout')
@section('title') Login - Booxtant @stop
@section('page-title')
Login
@stop
@section('page-content')
     <div class="main-content-container container">
                <div class="row">

                    <div class="col-md-12">
                        <div id="content" class="main-content-inner" role="main">

                            <article id="post-1709" class="post-1709 page type-page status-publish entry">

                                <div class="entry-content">

                                    <div class="woocommerce">

                                        <h2>Login</h2>

                                        @if (count($errors) > 0)
                                        <div class="alert alert-danger">
                                          <ul>
                                            @foreach ($errors->all() as $error)
                                              <li>{{ $error }}</li>
                                            @endforeach
                                          </ul>
                                        </div>
                                        @endif

                                        @if (Session::has('message'))
                                        <div class="alert alert-danger">{{ Session::get('message') }}</div>
                                        @endif

                                        {!! Form::open(array('route' => 'users.login', 'class' => 'login','autocomplete'=>'off')) !!}                                    
                                            <p class="woocommerce-FormRow woocommerce-FormRow--wide form-row form-row-wide">
                                            {!! Form::label('email', 'Email Address') !!}
                                            {!!Form::email('email',null, array('class' => 'woocommerce-Input woocommerce-Input--text input-text','required'))!!}
                                            </p>    

                                            <p class="woocommerce-FormRow woocommerce-FormRow--wide form-row form-row-wide">
                                                {!! Form::label('password', 'Password') !!}
                                                {!!Form::password('password', array('class' => 'woocommerce-Input woocommerce-Input--text input-text', 'id'=>'password1', 'minlength' => '8' , 'maxlength' => '12' ,'required'))!!}
                                            </p>

                                            <p class="form-row">

                                                <input type="submit" class="woocommerce-Button button" name="login" value="Login" />
                                                {{-- <label for="rememberme" class="inline">
                                                    <input class="woocommerce-Input woocommerce-Input--checkbox" name="rememberme" type="checkbox" id="rememberme" value="forever" /> Remember me </label> --}}
                                            </p>
                                         {!! Form::close()!!}

                                    </div>

                                </div>

                            </article>

                        </div>
                    </div>
                </div>
            </div>
@stop