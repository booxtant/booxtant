@extends('layout')
@section('title') Join - Booxtant @stop
@section('page-title')
Join Us
@stop
@section('page-content')
     <div class="main-content-container container">
                <div class="row">

                    <div class="col-md-12">
                        <div id="content" class="main-content-inner" role="main">

                            <article id="post-1709" class="post-1709 page type-page status-publish entry">

                                <div class="entry-content">

                                    <div class="woocommerce">

                                        <h2>Join Bookish</h2>

                                        @if (count($errors) > 0)
                                        <div class="alert alert-danger">
                                          <ul>
                                            @foreach ($errors->all() as $error)
                                              <li>{{ $error }}</li>
                                            @endforeach
                                          </ul>
                                        </div>
                                        @endif

                                        {!! Form::open(array('route' => 'users.store', 'class' => 'login','autocomplete'=>'off')) !!}
                                            <p class="woocommerce-FormRow woocommerce-FormRow--wide form-row form-row-wide">
                                            Already Registered ?<br/><a href="{{URL::route('login')}}">Click Here To Login</a>
                                            </p>
                                            <p class="woocommerce-FormRow woocommerce-FormRow--wide form-row form-row-wide">
                                            {!! Form::label('firstname', 'First Name') !!}
                                            {!!Form::text('firstname',null, array('class' => 'woocommerce-Input woocommerce-Input--text input-text', 'required'))!!}
                                            </p>

                                            <p class="woocommerce-FormRow woocommerce-FormRow--wide form-row form-row-wide">
                                            {!! Form::label('lastname', 'Last Name') !!}
                                            {!!Form::text('lastname',null, array('class' => 'woocommerce-Input woocommerce-Input--text input-text', 'required'))!!}
                                            </p>

                                            <p class="woocommerce-FormRow woocommerce-FormRow--wide form-row form-row-wide">
                                            {!! Form::label('email', 'Email Address') !!}
                                            {!!Form::email('email',null, array('class' => 'woocommerce-Input woocommerce-Input--text input-text','required'))!!}
                                            </p>	

                                            <p class="woocommerce-FormRow woocommerce-FormRow--wide form-row form-row-wide">
                                                {!! Form::label('password', 'Password') !!}
                                                {!!Form::password('password', array('class' => 'woocommerce-Input woocommerce-Input--text input-text', 'id'=>'password1', 'minlength' => '8' , 'maxlength' => '12' ,'required'))!!}
                                            </p>

                                            <p class="woocommerce-FormRow woocommerce-FormRow--wide form-row form-row-wide">
                                                {!! Form::label('password_confirmation', 'Confirm Password') !!}
                                                {!!Form::password('password_confirmation', array('class' => 'woocommerce-Input woocommerce-Input--text input-text', 'id'=>'password1', 'minlength' => '8' , 'maxlength' => '12' ,'required'))!!}
                                            </p>
                                            <p class="form-row">
                                                <input type="submit" class="woocommerce-Button button" name="join" value="Join Us" />
                                                {{-- <label for="rememberme" class="inline">
                                                    <input class="woocommerce-Input woocommerce-Input--checkbox" name="rememberme" type="checkbox" id="rememberme" value="forever" /> Remember me </label> --}}
                                            </p>
                                         {!! Form::close()!!}
                                        

                                    </div>

                                </div>

                            </article>

                        </div>
                    </div>
                </div>
            </div>
@stop