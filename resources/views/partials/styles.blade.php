<link rel='stylesheet' id='woocommerce-smallscreen-css' href='css/woocommerce-smallscreen.css' type='text/css' media='only screen and (max-width: 768px)' />
{!! Html::style('css/woocommerce-layout.css') !!}
{!! Html::style('css/woocommerce.css') !!}
{!! Html::style('css/bootstrap.min.css') !!}
{!! Html::style('css/jquery.smartmenus.bootstrap.css') !!}
{!! Html::style('https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css') !!}
{!! Html::style('assets/js_composer/assets/css/js_composer.min.css') !!}
{!! Html::style('assets/simple-icon/simple-line-icons.css') !!}
{!! Html::style('assets/drip-icon/webfont.css') !!}
{!! Html::style('css/style-theme.css') !!}
{!! Html::style('css/style-woocommerce.css') !!}
{!! Html::style('css/style-shortcodes.css') !!}
<!--[if lte IE 9]>
{!! Html::style('assets/js_composer/assets/css/vc_lte_ie9.min.css') !!}
<![endif]-->
<!--[if IE  8]>
{!! Html::style('assets/js_composer/assets/css/vc-ie8.min.css') !!}
<![endif]-->
{!! Html::style('assets/toko-shortcodes/vendors/owl-carousel/owl.carousel.css') !!}
{!! Html::style('assets/js_composer/assets/css/lib/vc-entypo/vc_entypo.min.css') !!}
{!! Html::style('css/dflip.css') !!}
{!! Html::style('css/themify-icons.css') !!}
{!! Html::style('css/custom.css') !!}
<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">
<link rel="icon" href="images/favicon.ico" type="image/x-icon">