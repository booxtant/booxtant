{!! Html::script('js/woocommerce/assets/js/jquery-blockui/jquery.blockUI.min.js') !!}
{!! Html::script('js/woocommerce/assets/js/frontend/woocommerce.min.js') !!}
{!! Html::script('js/woocommerce/assets/js/jquery-cookie/jquery.cookie.min.js') !!}
{!! Html::script('js/woocommerce/assets/js/frontend/cart-fragments.min.js') !!}
{!! Html::script('js/woocommerce/assets/js/frontend/woocommerce.min.js') !!}
{!! Html::script('js/vendor/bootstrap/js/bootstrap.min.js') !!}
{!! Html::script('js/vendor/smartmenu/jquery.smartmenus.min.js') !!}
{!! Html::script('js/script.js') !!}
{!! Html::script('js/toko-shortcodes/vendors/owl-carousel/owl.carousel.min.js') !!}
{!! Html::script('js/js_composer/assets/js/dist/js_composer_front.min.js') !!}
{!! Html::script('js/vendor/smartmenu/jquery.smartmenus.bootstrap.min.js') !!}
{!! Html::script('js/dflip.js') !!}
<!-- TokoPress JavaScript -->
<script type="text/javascript">
jQuery(function($) {
$('.toko-slider-active').owlCarousel({
items: 1,
loop: true,
nav: false,
lazyLoad: true,
autoplay: true,
autoplayHoverPause: true,
dots: true,
stopOnHover: true,
animateOut: 'fadeOut',
autoplayTimeout: 5000
});
});
</script>
<script>
    var flipBook;
    jQuery(document).ready(function () {
    	$(".showpdf").click(function(){
    		var pdfSrc = $(this).attr("data-pdf");
			pdf=pdfSrc;
        	var options = {hard:'none',webgl:false,height: 700, duration: 800};
        	flipBook = $("#flipbookContainer").flipBook(pdf, options);        	
		});

		$(".close").click(function(){
    		$( "#flipbookContainer" ).empty(); 	
		});


        
    });
</script>

