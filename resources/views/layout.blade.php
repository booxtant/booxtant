<!DOCTYPE html>
<html lang="en-US">

<head>
    @include('partials.meta') 
    @include('partials.fonts')
    @include('partials.styles') 
    @include('partials.head-scripts')
</head>

<body class="page page-id-1709 page-template page-template-page_fullwidth page-template-page_fullwidth-php woocommerce-account woocommerce-page header-large ltr wpb-js-composer js-comp-ver-4.12 vc_responsive">

    <div class="site-wrap">

        <header class="section-site-header">
            <div class="site-header">
                <div class="container">
                    <div class="site-brand pull-left">
                        <a class="navbar-brand" href="{{URL::route('index')}}">
                            <div class="site-logo-text">
                            <span class="vc_icon_element-icon entypo-icon entypo-icon-book-open logo-icon"></span>
                            </div>                                                            
                            <p class="site-description">Booxtant</p>
                        </a>
                    </div>

                    <div class="site-quicknav pull-right">
                        <ul class="nav navbar-right">

                            <li class="dropdown visible-xs visible-sm">
                                <a href="#" class="dropdown-toggle" data-toggle="collapse" data-target=".navbar-collapse-top">
                                    <i class="fa fa-navicon"></i>
                                </a>
                            </li>

                            @if (Auth::check()) 
                               <li class="dropdown">
                                <a href="#" class="dropdown-toggle cart-subtotal" data-toggle="dropdown" rel="nofollow">
                                    <span class="topnav-label"><span class="amount">Welcome, {{Auth::user()->firstname}}</span></span>
                                    <i class="simple-icon-user"></i>
                                </a>
                                <ul class="dropdown-menu topnav-minicart-dropdown sm-nowrap">
                                    <li class="menu-item"><a title="My Account" href="{{URL::route('account')}}">My Books</a></li>
                                    <li class="menu-item"><a title="My Account" href="{{URL::route('books.create')}}">Add A New Book</a></li>
                                    <li class="menu-item"><a title="My Account" href="{{URL::route('users.logout')}}">Logout</a></li>
                                </ul>
                                </li>       
                                @endif
                    
                                @if (!Auth::check())
                                <li class="dropdown">
                                <a href="#" class="dropdown-toggle cart-subtotal" data-toggle="dropdown" rel="nofollow">
                                    <span class="topnav-label"><span class="amount">Welcome, Guest</span></span>
                                    <i class="simple-icon-user"></i>
                                </a>
                                <ul class="dropdown-menu topnav-minicart-dropdown sm-nowrap">
                                    <li class="menu-item"><a title="My Account" href="{{URL::route('login')}}">SIGN IN</a></li>
                                    <li class="menu-item"><a title="My Account" href="{{URL::route('join')}}">JOIN</a></li>
                                </ul>
                                </li>    
                                @endif

                        </ul>
                    </div>

                    <div class="site-menu navbar-collapse collapse navbar-collapse-top ">
                        <ul id="menu-header-menu" class="site-menu nav navbar-nav">
                            <li class="menu-item"><a title="Home" href="{{URL::route('index')}}">Home</a>
                            </li>
                            <li class="menu-item"><a title="about" href="{{URL::route('index')}}">Who We Are</a>
                            </li>
                            <li class="menu-item"><a title="Bookstore" href="{{URL::route('explore')}}">Explore Books</a></li>
                        </ul>
                    </div>

                </div>
            </div>

        </header>
        <section id="page-title" class="page-title">
            <div class="container">

                <h1>@yield('page-title')</h1>

                <ol class="breadcrumb-trail breadcrumb breadcrumbs">
                    <li><span itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a itemprop="url" class="home" href="http://demo.toko.press/bookie2/dummy">Home</a></span></li>
                    <li class="active"><span itemscope itemtype="http://data-vocabulary.org/Breadcrumb" class="active"><span itemprop="title">@yield('page-title')</span></span>
                    </li>
                </ol>
            </div>
        </section>
            @yield('search-bar')
        <div class="main-content-container container">
        	@yield('page-content')
        </div>

        <footer class="section-site-footer">

            <div class="site-footer">
                <div class="container">
                    <div class="footer-credit">
                        <p>Copyright &copy; Booxtant - 2016</p>
                    </div>
                </div>
            </div>

        </footer>

    </div>
    @include('partials.bottom-scripts')

</body>

</html>