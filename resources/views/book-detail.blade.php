@extends('layout') 
@section('title') {{$book->name}} - Booxtant @stop
@section('page-title') {{$book->name}} @stop 
@section('search-bar')
<div class="books-search">
<div class="container">
    {!! Form::open(array('method'=>'GET','route' => 'search','autocomplete'=>'off')) !!}
    
        <div class="row">
            <div class="col-sm-9 col-md-9">
                <div class="form-group">
                {!! Form::text('search', null, array('required', 'class'=>'form-control','placeholder'=>'Search Books')) !!}
                    
                </div>
            </div>

            <div class="col-sm-3 col-md-3">
                <div class="form-group">
                {!! Form::submit('search',array('class'=>'btn btn-primary btn-block')) !!}
                </div>
            </div>

        </div>
    </form>
</div>
</div>
@stop @section('page-content')
<div id="content" class="main-content-inner" role="main">
    <div class=" product type-product status-publish has-post-thumbnail book_author-atkia product_cat-science product_tag-etnic product_tag-money product_tag-novel first instock sale shipping-taxable purchasable product-type-simple">
        <div class="images">
            <a href="#" itemprop="image" class="woocommerce-main-image zoom" title="">
                <img src="../{{$book->book_cover}}" class="attachment-shop_single size-shop_single wp-post-image" alt="" title="">
            </a>
        </div>
        <div class="summary entry-summary">
            <h1 itemprop="name" class="product_title entry-title">{{$book->name}}</h1>
            <div itemprop="description">
                <p>{{$book->description}}</p>
            </div>
            <div class="product-offer-box">
                    @if(Auth::check())
                    <a rel="nofollow" class="btn btn-danger showpdf" data-pdf="../{{$book->book_pdf}}" data-toggle="modal" data-target="#modal-fullscreen">Read Book</a>
                    @endif
                    @if(!Auth::check())
                    <a rel="nofollow" class="btn btn-danger showpdf" href="{{URL::route('join')}}">Join To Read</a>
                    @endif
            </div>
        </div>
        <div class="section-book-details clearfix">
            <div class="row">
                <div class="col-md-6">
                    <div class="book-authors">
                        <h3>Published By</h3>
                        <h4>{{$book->user->firstname}} {{$book->user->lastname}}</h4>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal modal-fullscreen fade" id="modal-fullscreen" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                </div>
                <div class="modal-body">
                    <div id="flipbookContainer"></div>
                </div>
            </div>
        </div>
    </div>
@stop