@extends('layout')
@section('title') Books - Booxtant @stop
@section('page-title')
Books
@stop
@section('search-bar')
<div class="books-search">
<div class="container">
    {!! Form::open(array('method'=>'GET','route' => 'search','autocomplete'=>'off')) !!}
    
        <div class="row">
            <div class="col-sm-9 col-md-9">
                <div class="form-group">
                {!! Form::text('search', null, array('required', 'class'=>'form-control','placeholder'=>'Search Books')) !!}
                    
                </div>
            </div>

            <div class="col-sm-3 col-md-3">
                <div class="form-group">
                {!! Form::submit('search',array('class'=>'btn btn-primary btn-block')) !!}
                </div>
            </div>

        </div>
    </form>
</div>
</div>
@stop

@section('page-content')
<div class="row">
    <div class="col-md-8">

        <div id="content" class="main-content-inner background-none" role="main">

            <ul class="products">
                @foreach ($books as $book)
                <li  class="col-md-4 post-60 product type-product status-publish has-post-thumbnail book_author-atkia product_cat-drama product_cat-love-story product_tag-money product_tag-novel product_tag-sound last instock shipping-taxable purchasable product-type-simple">
                    <div class="product-inner">
                        <a href="{{URL::to('books',array('id'=>$book->id))}}" class="woocommerce-LoopProduct-link">
                            <figure class="product-image-box"><img width="250" height="333" src="{{$book->book_cover}}" class="attachment-shop_catalog size-shop_catalog wp-post-image" alt="{{$book->name}}" title="{{$book->name}}" /></figure>
                            <div class="product-price-box clearfix">
                                <h6>{{$book->name}}</h6><span class="person-name vcard">{{$book->user->firstname}} {{$book->user->lastname}}</span>
                            </div>
                        </a>
                        <div class="woo-button-wrapper">
                            <div class="woo-button-border"><a href="{{URL::to('books',array('id'=>$book->id))}}" class="button product-button">Details</a></div>
                        </div>
                    </div>
                </li>
                @endforeach

            </ul>

            <nav class="paging-navigation">
             {{ $books->render()}}
            </nav>

        </div>

    </div>
    <div class="col-md-4">
        <aside id="sidebar" class="sidebar">
            
            <section id="woocommerce_top_rated_products-2" class="widget woocommerce widget_top_rated_products">
                <div class="widget-wrap widget-inside">
                    <h3 class="widget-title">Recently Added Books</h3>
                    <ul class="product_list_widget">
                        @foreach ($recentBooks as $recentBook)
                        <li>
                            <a href="{{URL::to('books',array('id'=>$book->id))}}" title="See Me">
                                <img width="102" height="136" src="{{$recentBook->book_cover}}" class="attachment-shop_thumbnail size-shop_thumbnail wp-post-image" alt="{{$recentBook->name}}" /> <span class="product-title">{{$recentBook->name}}</span>
                            </a>
                        </li>
                        @endforeach
                    </ul>
                </div>
            </section>
        </aside>
    </div>
</div>

@stop