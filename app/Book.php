<?php

namespace App;
use DB;
use Request;
use Validator;
use Auth;
use App\Http\Requests;
use Illuminate\Support\Facades\Hash;

use Illuminate\Database\Eloquent\Model;



class Book extends Model
{
    public function user(){
    	return $this->belongsTo('App\User');
    }

    protected $fillable=[ 'name',
    'user_id',
    'book_cover',
    'book_pdf',
    'description'
    ];

    public function rules()
    {
        return $rules = [
        'name'=> 'required|min:5|unique:books',
        'book_cover'=> 'required|mimes:png,jpeg',
        'book_pdf'=> 'required|mimes:pdf',
        'description'=>'required'
        ];
    }

    public function postBook() {

        $book = new Book;
        $validator = Validator::make(Request::all(), $this->rules());
        if($validator->passes())
        {
            $book->name = Request::get('name');
            $book->user_id = Auth::user()->id;
            $book->description = Request::get('description');
            $imagedestinationPath = 'uploads/images'; 
            $bookdestinationPath = 'uploads/books';
            $imgextension = Request::file('book_cover')->getClientOriginalExtension(); 
            $pdfextension = Request::file('book_pdf')->getClientOriginalExtension(); 
            $bookImage = $book->name.'.'.$imgextension;
            $bookPdf = $book->name.'.'.$pdfextension;
            Request::file('book_cover')->move($imagedestinationPath, $bookImage);
            Request::file('book_pdf')->move($bookdestinationPath, $bookPdf);
            $book->book_cover ="uploads/images/".$bookImage;
            $book->book_pdf ="uploads/books/".$bookPdf;
            $book->save();
        }
        else
        {
            return $validator;
        }
    }
}
