<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Book;

use Auth;

use App\Http\Requests;

class PagesController extends Controller
{
    public function index()
    {
        $books = Book::orderBy('created_at', 'DESC')->paginate(6);;
        return view('home')->with('books',$books);	
    }

    public function account()
    {
        $books = Book::where('user_id', Auth::user()->id)->get();
        return view('account')->with('books',$books);
    }

    public function explore()
    {
        $recentBooks = $books = Book::orderBy('created_at', 'desc')->take(6)->get();;
        $books = Book::orderBy('name', 'asc')->paginate(9);
        return view('books')->with('books',$books)->with('recentBooks',$recentBooks);
    }

    public function join()
    {
        return view('join');
    }

    public function login()
    {
        return view('login');
    }
}
