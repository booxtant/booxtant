<?php

namespace App\Http\Controllers;

use App\Book;
use Auth;
use Request;
use Session;
use Redirect;
use File;
use App\Http\Requests;


class BooksController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return View('create-book');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storeBook(Request $request)
    {
        $book = new Book;
        $validator = $book->postBook($request);
        
        if($validator)
        {
            return redirect()->back()->withInput()->with('errors',$validator->messages())->with('error_code','RV'); 
        }
        else
        {   
            return redirect('/account'); 
        }
    }

    public function search(Request $request)
    {
        $query = Request::input('search');

        $results = Book::where('name', 'LIKE', '%'.$query.'%')->get();
        
        return view('search')->with('results', $results);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
         $book = Book::find($id);

        // show the view and pass the nerd to it
        return view('book-detail')->with('book', $book);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $book = Book::findOrFail($id);
        File::delete($book->book_cover);
        File::delete($book->book_pdf);
        $book->delete();
        return redirect()->route('account');
    }
}
