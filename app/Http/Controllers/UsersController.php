<?php

namespace App\Http\Controllers;

use App\User;
use Auth;
use Request;
use Session;
use Redirect;
use App\Http\Requests;


class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return "Show All Users";
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return "Show All Users";
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = new User;
        $validator = $user->postUser($request);
        
        if($validator)
        {
            return redirect()->back()->withInput()->with('errors',$validator->messages())->with('error_code','RV'); 
        }
        else
        {   
            Auth::attempt(array('email'=>Request::get('email'),'password'=>Request::get('password')));
            return redirect('/account'); 
        }
    }

    public function login(Request $request)
    {
        $user = new User;
        $validator = $user->loginUser($request);
        if($validator)
        {
            return redirect()->back()->withInput()->with('errors',$validator->messages());
        }
        else
        {
            if(Auth::attempt(array('email'=>Request::get('email'),'password'=>Request::get('password'))))
            {
                return redirect('/account');    
            }

            else{
               return redirect()->back()->withInput()->with('message','Wrong Username/Password Combination');
            }
        }
    }

    public function logout()
    {
        // Session::flush();
        Auth::logout();
        return redirect('/');  
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
