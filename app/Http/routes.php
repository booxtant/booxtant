<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', [ 'as'=> 'index', 'uses'=> 'PagesController@index']);
Route::post('users/login', [ 'as'=> 'users.login', 'uses'=> 'UsersController@login']);
Route::get('/explore', [ 'as'=> 'explore', 'uses'=> 'PagesController@explore']);
Route::get('search', [ 'as'=> 'search', 'uses'=> 'BooksController@search']);



Route::group(['middleware' => 'guest'], function()		
	{
	Route::get('/join', [ 'as'=> 'join', 'uses'=> 'PagesController@join']);
        Route::get('/login', [ 'as'=> 'login', 'uses'=> 'PagesController@login']);
	});



Route::group(['middleware' => 'auth'], function()		
	{
	Route::get('users/logout', [ 'as'=> 'users.logout', 'uses'=> 'UsersController@logout']);
	Route::get('/account', [ 'as'=> 'account', 'uses'=> 'PagesController@account']);
	Route::post('books/store', [ 'as'=> 'storeBook', 'uses'=> 'BooksController@storeBook']);
	Route::resource('books', 'BooksController');
	});



Route::get('books/{books}',['as'=>'books.show','uses'=> 'BooksController@show']);

Route::resource('users', 'UsersController');
