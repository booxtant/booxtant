<?php

namespace App;
use DB;
use Request;
use Validator;
use Auth;
use App\Http\Requests;
use Illuminate\Support\Facades\Hash;

use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable=[ 'firstname',
    'lastname',
    'email',
    'password'
    ];

    public function setPasswordAttribute($password)
    {   
        $this->attributes['password'] = bcrypt($password);
    } 

    public function rules()
    {
        return $rules = [
        'firstname'=> 'required|min:2|alpha',
        'lastname'=> 'required|min:2|alpha',
        'email'=> 'required|email|unique:users',
        'password'=> 'required|alpha_num|between:8,12|confirmed',
        'password_confirmation'=> 'required'
        ];
    }

    public function loginRules()
    {
        return $rules = [
        'email'=> 'required|email',
        'password'=> 'required|alpha_num|between:8,12'
        ];
    }

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function postUser() {
        $user = new User;
        $validator = Validator::make(Request::all(), $this->rules());
        if($validator->passes())
        {
            User::create(Request::all());
        }
        else
        {
            return $validator;
        }
    }

    public function loginUser() {
        $validator = Validator::make(Request::all(), $this->loginRules());
        if(!$validator->passes())
        {   
            return $validator;
        }

    }
}
