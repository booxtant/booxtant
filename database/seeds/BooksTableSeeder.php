<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class BooksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('books')->insert(
            array(
                    array(
                        'name' => 'The Da Vinci Code 1',
						'user_id' => 1,
                        'book_cover' =>'uploads/images/My First Book.jpg',
                        'book_pdf' =>'uploads/book-15-250x333.pdf',
						'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                        'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
                        ),
                    array(
                        'name' => 'The Da Vinci Code 2',
                        'user_id' => 1,
                        'book_cover' =>'uploads/images/My First Book.jpg',
                        'book_pdf' =>'uploads/book-15-250x333.pdf',
                        'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                        'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
                        ),
                    array(
                        'name' => 'The Da Vinci Code 3',
                        'user_id' => 1,
                        'book_cover' =>'uploads/images/My First Book.jpg',
                        'book_pdf' =>'uploads/book-15-250x333.pdf',
                        'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                        'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
                        ),
                    array(
                        'name' => 'The Da Vinci Code 4',
                        'user_id' => 1,
                        'book_cover' =>'uploads/images/My First Book.jpg',
                        'book_pdf' =>'uploads/book-15-250x333.pdf',
                        'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                        'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
                        ),
                    array(
                        'name' => 'The Da Vinci Code 5',
                        'user_id' => 1,
                        'book_cover' =>'uploads/images/My First Book.jpg',
                        'book_pdf' =>'uploads/book-15-250x333.pdf',
                        'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                        'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
                        ),
                    array(
                        'name' => 'The Da Vinci Code 6',
                        'user_id' => 1,
                        'book_cover' =>'uploads/images/My First Book.jpg',
                        'book_pdf' =>'uploads/book-15-250x333.pdf',
                        'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                        'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
                        ),
                    array(
                        'name' => 'The Da Vinci Code 7',
                        'user_id' => 1,
                        'book_cover' =>'uploads/images/My First Book.jpg',
                        'book_pdf' =>'uploads/book-15-250x333.pdf',
                        'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                        'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
                        ),
                    array(
                        'name' => 'The Da Vinci Code 8',
                        'user_id' => 1,
                        'book_cover' =>'uploads/images/My First Book.jpg',
                        'book_pdf' =>'uploads/book-15-250x333.pdf',
                        'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                        'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
                        ),
                    array(
                        'name' => 'The Da Vinci Code 9',
                        'user_id' => 1,
                        'book_cover' =>'uploads/images/My First Book.jpg',
                        'book_pdf' =>'uploads/book-15-250x333.pdf',
                        'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                        'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
                        ),
                    array(
                        'name' => 'The Da Vinci Code 10',
                        'user_id' => 1,
                        'book_cover' =>'uploads/images/My First Book.jpg',
                        'book_pdf' =>'uploads/book-15-250x333.pdf',
                        'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                        'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
                        )


                ));
    }
}
