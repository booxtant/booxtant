<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert(
            array(
                    array(
                        'firstname' => 'Ashish',
						'lastname' => 'Sharma', 
						'email' => 'sharma.asmith7@gmail.com',
						'password' => Hash::make('ASHISH1232'),
                        'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                        'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
                        ),

                    array(
                        'firstname' => 'Akanksha',
                        'lastname' => 'Sharma', 
                        'email' => 'sharma.akanksha@gmail.com',
                        'password' => Hash::make('ASHISH1232'),
                        'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                        'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
                        ),

                    array(
                        'firstname' => 'Shalini',
                        'lastname' => 'Kashyap', 
                        'email' => 'shalini@gmail.com',
                        'password' => Hash::make('ASHISH1232'),
                        'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                        'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
                        ),

                ));
    }
}
