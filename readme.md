# Booxtant Web Application


Booxtant is a Web Application primarily coded with a Laravel 5 Backend & Modern UI Technologies such as HTML5, CSS3, JQuery.
This Web Application helps amateur writers to publish their work and promote to large audience.
This application is built with the support from 2 major Open Source Libraries Turn.JS and PDF.JS.


## Official Documentation

To check the official Website [Booxtant website](http://booxtant.com).


## Developed By

Shalini Kashyap - S.D College, Ambala Cantt
Akanksha Sharma - S.D College, Ambala Cantt

## License
The Framework that we used to build this application uses the following framework.
The Laravel framework is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT).
